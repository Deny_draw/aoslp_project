<!-- resources/views/common/errors.blade.php -->

@if ($errors->any())

  <div class="alert alert-danger">
    <strong>Упс, кажется, что-то пошло не так.</strong>

    <br>

    <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif