@extends('layouts.app')

@section('content')
<div class="panel-body mt-4 mb-4 container w-50">

    @include('common.errors')

    <form action="{{ route('manage_destroy_user', $user->id) }}" method="POST" class="form-horizontal text-center">
        <div class="card">

            {{ csrf_field() }}
            {{ method_field('DELETE') }}

            <div class="card-header">
                Предупреждение
            </div>

            <div class="card-body">
                <div class="form-group">
                    <label for="task" class="control-label">Вы точно хотите удалить пользователя <b>{{ $user->name }}</b>?</label>
                </div>

                <div class="form-group">
                    <div class="">
                        <a class="btn btn-primary" href="{{ route('manage_users') }}" role="button" style="border-color: #bababa;">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z" />
                            </svg> Назад
                        </a>
                        <button type="submit" class="btn btn-danger" style="border-color: #bababa;">
                            <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-trash" fill="white" xmlns="http://www.w3.org/2000/svg">
                                <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                                <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                            </svg> Удалить
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
