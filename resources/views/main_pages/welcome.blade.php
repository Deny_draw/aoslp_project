@extends('layouts.app')

@section('content')

<div class="baskgr d-flex flex-row align-items-center justify-content-center mb-4">
    <div class="col-4 d-flex flex-column justify-content-center">
        <h5 style="text-align: center">Добро пожаловать на сайт СтудПортал</h5>
        <p class="caption">Здесь вы можете поделиться любыми новостями, секретами и вопросами, имеющими то или иное
            отношение к Московскому Политехническому Университету.
        </p>
        <div class="d-flex flex-row align-items-center justify-content-center">
            <a class="btn btn-success mr-3" href="{{ url('/login') }}">
                Войти
            </a>
            <a class="btn btn-primary" href="{{ url('/register') }}">
                Регистрация
            </a>
        </div>
    </div>
    <div class="vl"></div>
    <div class="col-4">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="http://assets.std-1223.ist.mospolytech.ru/rafiki.svg">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="http://assets.std-1223.ist.mospolytech.ru/rafiki2.svg">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="http://assets.std-1223.ist.mospolytech.ru/rafiki3.svg">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="http://assets.std-1223.ist.mospolytech.ru/rafiki4.svg">
                </div>
            </div>
        </div>
    </div>
</div>

@endsection