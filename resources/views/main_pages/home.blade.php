@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row mt-3 d-flex flex-lg-row-reverse flex-md-row-reverse flex-sm-col">
        <div class="col-lg-4 col-md-4 col-sm-12 mb-4">
            <div class="card text-center mb-3">
                <div class="card-body">
                    <h5 class="card-title mb-0">Создать новый пост</h5>
                    <hr class="featurette-divider mt-2 mb-2">
                    <a class="btn btn-default create-post" href="{{ url('/home/create') }}" role="button" style="border-color: #bababa;">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-plus" fill="dark-green" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
                        </svg> Создать пост
                    </a>
                </div>
            </div>
            <form class="card text-center my-2 my-lg-0 w-100" method="get" action="{{route('index_filtered')}}">
                <div class="card-body">
                    <h5 class="card-title mb-0">Фильтрация</h5>
                    <hr class="featurette-divider mt-2 mb-2">
                    <div class="d-flex align-items-center w-100 mb-2">
                        <input class="form-control form-control-custom w-100" type="search" id="search_title" placeholder="Поиск по названию поста" aria-label="Search" name="search_title" @if ($request) value="{{$request->search_title}}" @endif>
                    </div>
                    <div class="d-flex align-items-center w-100  mb-2">
                        <select name="search_theme" class="form-control form-control-custom w-100 custom-select" style="overflow: auto" type="search">
                            <option value="" selected>Поиск по теме</option>
                            @foreach($themes as $theme)
                            <option value="{{$theme->id}}" @if ($request and $request->search_theme != "" and $request->search_theme == $theme->id) selected @endif>{{$theme->theme}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="d-flex align-items-center w-100  mb-2">
                        <select name="search_author" class="form-control form-control-custom w-100 custom-select" style="overflow: auto" type="search">
                            <option value="" selected>Поиск по автору</option>
                            @foreach($users as $user)
                            <option value="{{$user->id}}" @if ($request and $request->search_author != "" and $request->search_author == $user->id) selected @endif>{{$user->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <button class="btn btn-primary d-flex align-items-center m-auto" type="submit">
                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="currentColor" class="bi bi-search mr-2" viewBox="0 0 16 16" style="margin-top: 1px">
                            <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z" />
                            <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z" />
                        </svg>
                        Поиск</button>
                </div>
            </form>
        </div>
        @if (count($posts) > 0)
        <div class="col-md-8 col-lg-8 col-sm-12">
            @foreach ($posts as $post)
            <div class="card mb-4">
                <div class="card-header d-flex justify-content-between">
                    <div class="d-flex col-9 pl-0">
                        <b class="">
                            <p class="mb-0">Автор:
                                @if($post->author)<a href="{{ route('profile', $post->user_id) }}">{{ $post->author->name}}</a>
                                @else
                                <b style="color: #717171; text-decoration: line-through;">Пользователь потерялся в корпусе на ПК</b>
                                @endif
                            </p>
                        </b>
                        <p class="ml-4 mb-0 text-muted">{{ $post->created_at }}</p>
                    </div>
                    @if(($post->author && ($post->author->id == Auth::user()->id || Auth::user()->user_privilege->is_admin)) || (!$post->author && Auth::user()->user_privilege->is_admin))
                    <div class="col-3 text-right pr-0">
                        <a href="{{ route('posts.edit', $post->id) }}" class="mr-1"><svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-pencil-square" fill="green" xmlns="http://www.w3.org/2000/svg">
                                <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" />
                                <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z" />
                            </svg></a>
                        <a href="{{ route('delete_post', $post->id) }}" class=""><svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-trash" fill="red" xmlns="http://www.w3.org/2000/svg">
                                <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                                <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                            </svg></a>
                    </div>
                    @endif
                </div>
                <div class="card-body">
                    <h5 class="card-title">{{ $post->title }}</h5>
                    <h6 class="card-text"> Тема: {{ $post->themes->theme }}</h6>
                    <div class="post-content">
                        <pre class="card-text">{{ $post->post_content }}</pre>
                    </div>
                </div>
                <div class="card-footer text-muted d-flex justify-content-between">
                    <a href="{{ route('posts.show', $post->id) }}" class="btn btn-success">Подробнее</a>
                    <div><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chat-right-text" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M2 1h12a1 1 0 0 1 1 1v11.586l-2-2A2 2 0 0 0 11.586 11H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1zm12-1a2 2 0 0 1 2 2v12.793a.5.5 0 0 1-.854.353l-2.853-2.853a1 1 0 0 0-.707-.293H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12z" />
                            <path fill-rule="evenodd" d="M3 3.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zM3 6a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9A.5.5 0 0 1 3 6zm0 2.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5z" />
                        </svg> {{ count($post->comments) }}</div>
                </div>
            </div>
            @endforeach

            {{ $posts->links("pagination::bootstrap-4") }}
        </div>
        @else
        <div class="col-md-8 col-lg-8 col-sm-12" style="text-align: center;">
            <h4>Посты не найдены</h4>
        </div>
        @endif
    </div>
</div>
@endsection