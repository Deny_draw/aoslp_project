@extends('layouts.app')

@section('content')

<div class="panel-body mt-4 mb-4 container w-50">

  @include('common.errors')

  <form action="{{ route('posts.update', $post->id) }}" method="POST" class="form-horizontal text-center">
    <div class="card">

      {{ csrf_field() }}
      {{ method_field('PUT') }}

      <div class="card-header">
        Редактировать пост
      </div>

      <div class="card-body">
        <div class="form-group">
          <label for="task" class="control-label">Название</label>

          <div class="">
            <input type="text" name="title" id="task-name" class="form-control" value="{{$post->title}}">
          </div>

          <label for="task" class="control-label">Тема</label>

          <!-- Написать логику для темы -->
          <select name="themes[]" class="form-control custom-select" id="themes" style="overflow: auto" >
            @foreach($themes as $theme)
            <option value="{{$theme->id}}" @if ($theme->id === $post->theme_id)
              selected
              @endif>{{$theme->theme}}</option>
            @endforeach
          </select>

          <label for="task" class="control-label">Содержание</label>

          <div class="">
            <textarea type="text" name="post_content" id="post-content" class="form-control post-content">{{$post->post_content}}</textarea>
          </div>
        </div>

        <div class="form-group">
          <div class="">
            <a class="btn btn-primary" href="{{ url()->previous() }}" role="button" style="border-color: #bababa;">
              <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z" />
              </svg> Назад
            </a>
            <button type="submit" class="btn btn-warning" style="border-color: #bababa;">
              <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-plus" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
              </svg> Сохранить изменения
            </button>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
@endsection