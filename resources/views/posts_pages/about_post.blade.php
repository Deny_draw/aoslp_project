@extends('layouts.app')

@section('content')
<div class="panel-body mt-4 mb-4 container">
    <div class="card text-center">

        <div class="card-header">
            <div class="d-flex flex-row justify-content-center">
                <h5 class="mb-0">{{ $post->title }}</h5>
                @if(($post->author && ($post->author->id == Auth::user()->id || Auth::user()->user_privilege->is_admin)) || (!$post->author && Auth::user()->user_privilege->is_admin))

                <a href="{{ route('posts.edit', $post->id) }}" class="ml-1 mr-1"><svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-pencil-square" fill="green" xmlns="http://www.w3.org/2000/svg">
                        <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" />
                        <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z" />
                    </svg></a>
                <a href="{{ route('delete_post', $post->id) }}" class=""><svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-trash" fill="red" xmlns="http://www.w3.org/2000/svg">
                        <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                        <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                    </svg></a>

                @endif
            </div>

        </div>

        <div class="card-body">
            <div class="form-group mb-1">
                <h5 class="mb-3">{{ $post->themes->theme }}</h5>
                <div class="about-post-content">
                    <pre>{{ $post->post_content }}</pre>
                </div>
            </div>
        </div>
        <div class="card-footer text-muted">
            <h6>Автор поста: {{ $post->author->name or 'Пользователь потерялся в корпусе на ПК'}}</h6>
            <label>дата последнего обновления: {{ $post->updated_at }}</label>
            <div class="form-group mb-1">
                <a class="btn btn-primary" href="{{ url('/home') }}" role="button" style="border-color: #bababa;">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z" />
                    </svg> Вернуться назад
                </a>
            </div>
        </div>
    </div>
    <div class="mt-3 mb-5">
        <h4>Комментарии к посту:</h4>
        <form action="{{ route('add_comment', $post->id) }}" method="POST" class="form-horizontal text-center">
            {{ csrf_field() }}
            {{ method_field('POST') }}
            <div class="mb-3 mt-3 w-100 d-flex align-items-center justify-content-between">
                <div class="" style="white-space: nowrap;">Добавить комментарий</div>
                <div class="input-group comment-input">
                    <input type="text" name="comment_content" required class="form-control ml-3" placeholder="Введите комментарий">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="submit">Отправить
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-forward-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path d="M9.77 12.11l4.012-2.953a.647.647 0 0 0 0-1.114L9.771 5.09a.644.644 0 0 0-.971.557V6.65H2v3.9h6.8v1.003c0 .505.545.808.97.557z" />
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        </form>

        @if ($comments)

        @foreach ($comments as $comment)

        <div class="comment mb-3">
            <div class="p-3">
                <div class="d-flex w-100">
                    <div class="d-flex justify-content-between flex-grow-1 ">
                        <b class="">
                            <p class="mb-0">Автор:
                                @if($comment->author)<a href="{{ route('profile', $comment->user_id) }}">{{ $comment->author->name}}</a>
                                @else
                                <b style="color: #717171; text-decoration: line-through;">Пользователь потерялся в корпусе на ПК</b>
                                @endif
                            </p>
                        </b>
                        <p class="mb-0 text-muted">{{ $comment->updated_at }}</p>
                    </div>
                    @if($comment->author && (Auth::user() and $comment->user_id == Auth::user()->id || Auth::user()->user_privilege->is_admin))
                    <div class="ml-3 flex-shrink-1">
                        <a href="{{ route('edit_comment', ['id' => $comment->post_id, 'comment_id' => $comment->id]) }}" class="mr-1"><svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-pencil-square" fill="green" xmlns="http://www.w3.org/2000/svg">
                                <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" />
                                <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z" />
                            </svg></a>
                        <a href="{{ route('delete_comment', ['id' => $comment->post_id, 'comment_id' => $comment->id]) }}" class=""><svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-trash" fill="red" xmlns="http://www.w3.org/2000/svg">
                                <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                                <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                            </svg></a>
                    </div>
                    @elseif(!$comment->author && Auth::user()->user_privilege->is_admin)
                    <div class="ml-3 flex-shrink-1">
                        <a href="{{ route('delete_comment', ['id' => $comment->post_id, 'comment_id' => $comment->id]) }}" class=""><svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-trash" fill="red" xmlns="http://www.w3.org/2000/svg">
                                <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                                <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                            </svg></a>
                    </div>
                    @endif
                </div>
                <div class="mt-2 mb-2">
                    <div class="">
                        <p class="card-text">{{ $comment->comment_content }}</p>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        {{ $comments->links("pagination::bootstrap-4") }}
        @else
        <div>У этого поста пока нет комментариев, но Ваш может стать первым!</div>
        @endif
    </div>
</div>
@endsection