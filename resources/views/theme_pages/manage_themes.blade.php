@extends('layouts.app')

@section('content')
<div class="container">
    <div class="panel-body mt-4 mb-4">
        <div class="card text-center">
            <div class="card-header">
                <h5 class="card-title">Добавить новую тему</h5>
            </div>
            <div class="card-body">
                <a class="btn btn-primary" href="{{ url('/home') }}" role="button" style="border-color: #bababa;">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z" />
                    </svg> Вернуться на главную
                </a>
                <a class="btn btn-default" href="{{ route('manage_create_theme') }}" role="button" style="border-color: #bababa;">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-plus" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
                    </svg> Добавить
                </a>
            </div>
        </div>
    </div>

    @if (count($themes) > 1)
    <div class="card mb-4">
        <div class="panel panel-default">
            <div class="panel-heading mt-3 text-center">
                <h5>Темы</h5>
            </div>

            <div class="card-body" style="overflow: auto;">
                <table class="table table-striped task-table">
                    <thead>
                        <th>тема</th>
                        <th class="text-center">удалить</th>
                        <th class="text-center">изменить</th>
                    </thead>

                    <tbody>
                        @foreach ($themes as $theme)
                        @if ($theme->id != 0)
                        <tr>

                            <td>
                                <div> <a href="{{ route('manage_about_theme', $theme->id) }}"> {{ $theme->theme }} </a> </div>
                            </td>

                            <td class="text-center">
                                <a href="{{ route('manage_delete_theme', $theme->id) }}" class="mr-1">
                                    <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-trash" fill="red" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                                        <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                                    </svg>
                                </a>
                            </td>

                            <td class="text-center">
                                <a href="{{ route('manage_edit_theme', $theme->id) }}" class="" style="width:100px;">
                                    <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-pencil-square" fill="green" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" />
                                        <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z" />
                                    </svg>
                                </a>
                            </td>
                        </tr>
                        @endif
                        @endforeach
                    </tbody>
                </table>
                {{ $themes->links("pagination::bootstrap-4") }}
            </div>
        </div>
    </div>
    @else
    <div style="text-align: center;">
        <h4>На данном сайте пока нет тем, но ваша может стать первой!</h4>
    </div>
    @endif
</div>
@endsection