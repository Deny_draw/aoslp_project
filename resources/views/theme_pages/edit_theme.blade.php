@extends('layouts.app')

@section('content')
<div class="panel-body mt-4 mb-4 container w-75">

    @include('common.errors')

    <form action="{{ route('manage_update_theme', $theme->id) }}" method="POST" class="form-horizontal text-center">
        <div class="card">

            {{ csrf_field() }}
            {{ method_field('PUT') }}

            <div class="card-header">
                Изменить название темы
            </div>

            <div class="card-body">
                <div class="form-group">
                    <div class="">
                        <input type="text" name="theme" id="task-name" class="form-control" value="{{$theme->theme}}">
                    </div>
                </div>

                <div class="form-group">
                    <div class="">
                        <a class="btn btn-primary" href="{{ url()->previous() }}" role="button" style="border-color: #bababa;">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z" />
                            </svg> Назад
                        </a>
                        <button type="submit" class="btn btn-warning" style="border-color: #bababa;">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-plus" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
                            </svg> Сохранить изменения
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
