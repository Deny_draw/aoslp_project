@extends('layouts.app')

@section('content')
<div class="panel-body mt-4 mb-4 container">
    <div class="card text-center">

        <div class="card-header">
            <h5>Имя пользователя: {{ $user->name }}</h5>
        </div>

        <div class="card-body">
            <div class="form-group">
                <h6>E-mail: {{ $user->email }}</h6>
            </div>
            <div class="form-group">
                <h6>День рождения: {{ $user->user_info->birth_date or 'неизвестен'}}</h6>
            </div>

            <div class="form-group">
                <h6>Телефон: {{ $user->user_info->telephone or 'неизвестен'}}</h6>
            </div>

            <div class="form-group">
                <h6>Статус: {{ $user->user_info->status or 'неизвестен'}}</h6>
            </div>
        </div>
        <div class="card-footer text-muted">
            <div class="form-group">
                <a class="btn btn-primary" href="{{ url('/home') }}" role="button" style="border-color: #bababa;">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z" />
                    </svg> Вернуться назад
                </a>
            </div>
        </div>
    </div>
</div>
@endsection
