<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function user_info()
    {
        return $this->hasOne('App\UserInfo');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function user_privilege()
    {
        return $this->hasOne('App\UsersPrivilege');
    }
}
