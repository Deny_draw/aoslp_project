<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Post;
use App\Theme;
use App\User;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $posts = Post::orderBy('created_at', 'desc')->paginate(5);
        $themes = Theme::all();
        $users = User::all();
        return Auth::check() ? view('main_pages.home', [
            'posts' => $posts,
            'themes' => $themes,
            'users' => $users,
            'request' => NULL,
            'user' => Auth::user(),
        ]) : redirect('/login');
    }

    public function index_filtered(Request $request)
    {
        if ($request->search_title or $request->search_theme != "" or $request->search_author != "") {
            $posts = Post::orderBy('created_at', 'desc');
            if ($request->search_title) {
                $str = $request->search_title;
                $posts = $posts->where('title', 'like', '%' . $str . '%');
            }
            if ($request->search_theme != "") {
                $str = $request->search_theme;
                $posts = $posts->where("theme_id", $str);
            }
            if ($request->search_author != "") {
                $str = $request->search_author;
                $posts = $posts->where("user_id", $str);
            }
            $themes = Theme::all();
            $users = User::all();
            return Auth::check() ? view('main_pages.home', [
                'posts' => $posts->paginate(5),
                'themes' => $themes,
                'users' => $users,
                'user' => Auth::user(),
                'request' => $request,
            ]) : redirect('/login');
        } else {
            return redirect("/home");
        }
    }
}
