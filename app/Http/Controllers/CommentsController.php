<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CommentRequest;
use App\Comment;

class CommentsController extends Controller
{
    public function add_comment(CommentRequest $request, $id)
    {
        $user = Auth::user();
        $comment = new Comment;
        $comment->comment_content = $request->comment_content;
        $comment->post_id = $id;
        $comment->user_id = $user->id;
        $comment->save();
        return Auth::check() ? redirect()->back() : redirect('/login');
    }

    public function edit_comment($id, $comment_id)
    {
        $comment = Comment::find($comment_id);
        return Auth::check() ? view('comments_pages.edit_comment', [
            'comment' => $comment,
        ]) : redirect('/login');
    }

    public function update_comment(CommentRequest $request, $id, $comment_id)
    {
        $comment = Comment::find($comment_id);
        if ($comment->comment_content != $request->comment_content) {
            $comment->comment_content = $request->comment_content;
            $comment->save();
        }
        return Auth::check() ? redirect()->route('posts.show', ['id' => $id]) : redirect('/login');
    }

    public function delete_comment($comment_id)
    {
        $comment = Comment::find($comment_id);
        return Auth::check() ? view('comments_pages.delete_comment', [
            'comment' => $comment,
        ]) : redirect('/login');

    }

    public function destroy_comment($id, $comment_id)
    {
        $comment = Comment::find($comment_id);
        $comment->delete();
        return Auth::check() ? redirect()->route('posts.show', ['id' => $id]) : redirect('/login');
    }
}
