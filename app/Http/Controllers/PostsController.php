<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use Illuminate\Support\Facades\Auth;
use App\Post;
use App\Comment;
use App\Theme;

class PostsController extends Controller
{
    public function main_page()
    {
        if (Auth::guest())
            return view('main_pages.welcome');
        else
            return redirect('/home');
    }


    public function create()
    {
        $themes = Theme::all();
        return view('posts_pages.create_post', ['themes' => $themes]);
    }

    public function store(PostRequest $request)
    {
        $user = Auth::user();
        $post = new Post;
        $post->title = $request->title;
        $post->post_content = $request->post_content;
        $post->theme_id = $request->themes[0];
        $post->user_id = $user->id;
        $post->save();
        return redirect('/home');
    }

    public function show(Post $post)
    {
        $comments = Comment::where('post_id', $post->id)->orderBy('created_at', 'desc')->paginate(10);
        return Auth::check() ? view('posts_pages.about_post', ['post' => $post, 'comments' => $comments]) : redirect('/login');
    }

    public function edit(Post $post)
    {
        $themes = Theme::all();
        return Auth::check() ? view('posts_pages.edit_post', [
            'post' => $post,
            'themes' => $themes,
        ]) : redirect('/login');
    }

    public function update(PostRequest $request, Post $post)
    {
        if ($post->title != $request->title || $post->theme != $request->theme || $post->post_content != $request->post_content || $post->theme_id != $request->themes[0]) {
            $post->title = $request->title;
            $post->theme_id = $request->themes[0];
            $post->post_content = $request->post_content;
            $post->save();
        }
        return Auth::check() ? redirect(route('posts.show', $post->id) ) : redirect('/login');
    }

    public function delete($id)
    {
        $post = Post::find($id);
        return Auth::check() ? view('posts_pages.delete_post', [
            'post' => $post,
        ]) : redirect('/login');
    }

    public function destroy(Post $post)
    {
        $post->comments()->delete();
        $post->delete();
        return Auth::check() ? redirect('/home') : redirect('/login');
    }
}
