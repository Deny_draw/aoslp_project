<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Requests\AdminUpdateUserRequest;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\ThemeRequest;
use App\UsersPrivilege;
use App\User;
use App\UserInfo;
use App\Theme;

class AdminController extends Controller
{
    public function show_manage_users()
    {
        $users = User::where('id', '<>', Auth::id())->orderBy('created_at', 'asc')->paginate(10);
        if (Auth::check()) {
            return Auth::user()->user_privilege->is_admin ? view('manage_users_pages.manage_users', ['users' => $users]) : redirect()->back();
        } else
            return redirect('/login');
    }

    public function manage_create_user()
    {
        if (Auth::check()) {
            return Auth::user()->user_privilege->is_admin ? view('manage_users_pages.manage_add_user') : redirect()->back();
        } else
            return redirect('/login');
    }

    public function manage_about_user($id)
    {
        $user = User::find($id);
        if (Auth::check()) {
            return Auth::user()->user_privilege->is_admin ? view('manage_users_pages.manage_about_user', [
                'user' => $user,
            ]) : redirect()->back();
        } else
            return redirect('/login');
    }

    public function manage_add_user(CreateUserRequest $request)
    {
        echo $request;
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);
        $user_info = new UserInfo;
        $user_privilege = new UsersPrivilege;
        $user_privilege->is_admin = $request->user;
        $user_privilege->user_id = $user->id;
        $user_privilege->save();
        if (is_null($request->telephone) || $request->telephone == "")
            $request->telephone = NULL;
        if (is_null($request->status) || $request->status == "")
            $request->status = NULL;
        if (is_null($request->birth_date) || $request->birth_date == "")
            $request->birth_date = NULL;
        $user_info->birth_date = $request->birth_date;
        $user_info->telephone = $request->telephone;
        $user_info->status = $request->status;
        $user_info->user_id = $user->id;
        $user_info->save();
        if (Auth::check()) {
            return Auth::user()->user_privilege->is_admin ? redirect('manage_users') : redirect()->back();
        } else
            return redirect('/login');
    }

    public function manage_edit_user($id)
    {
        $user = User::find($id);
        if (Auth::check()) {
            return Auth::user()->user_privilege->is_admin ? view('manage_users_pages.manage_edit_user', [
                'user' => $user,
            ]) : redirect()->back();
        } else
            return redirect('/login');
    }

    public function manage_update_user(AdminUpdateUserRequest $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = $user->password;
        $user->save();
        if (is_null($request->birth_date) || $request->birth_date == "")
            $request->birth_date = NULL;
        $user->user_info->birth_date = $request->birth_date;
        $user->user_info->telephone = $request->telephone;
        $user->user_info->status = $request->status;
        $user->user_info->user_id = $user->id;
        $user->user_info->save();
        $user->user_privilege->is_admin = $request->user;
        $user->user_privilege->save();
        if (Auth::check()) {
            return Auth::user()->user_privilege->is_admin ? redirect('manage_users') : redirect()->back();
        } else
            return redirect('/login');
    }

    public function manage_delete_user($id)
    {
        $user = User::find($id);
        if (Auth::check()) {
            return Auth::user()->user_privilege->is_admin ? view('manage_users_pages.manage_delete_user', [
                'user' => $user,
            ]) : redirect()->back();
        } else
            return redirect('/login');
    }

    public function manage_destroy_user($id)
    {
        $user = User::find($id);
        foreach ($user->comments as $comment) {
            $comment->user_id = NULL;
            $comment->save();
        }
        foreach ($user->posts as $post) {
            $post->user_id = NULL;
            $post->save();
        }
        $user->user_info->delete();
        $user->user_privilege->delete();
        $user->delete();
        if (Auth::check()) {
            return Auth::user()->user_privilege->is_admin ? redirect('manage_users') : redirect()->back();
        } else
            return redirect('/login');
    }

    public function show_manage_themes()
    {
        $themes = Theme::orderBy('created_at', 'dec')->paginate(10);
        if (Auth::check()) {
            return Auth::user()->user_privilege->is_admin ? view('theme_pages.manage_themes', ['themes' => $themes]) : redirect()->back();
        } else
            return redirect('/login');
    }

    public function manage_create_theme()
    {
        if (Auth::check()) {
            return Auth::user()->user_privilege->is_admin ? view('theme_pages.create_theme') : redirect()->back();
        } else
            return redirect('/login');
    }

    public function manage_add_theme(ThemeRequest $request)
    {
        Theme::create([
            'theme' => $request->theme,
        ]);
        if (Auth::check()) {
            return Auth::user()->user_privilege->is_admin ? redirect('manage_themes') : redirect()->back();
        } else
            return redirect('/login');
    }

    public function manage_about_theme($id)
    {
        $theme = Theme::find($id);
        if (Auth::check()) {
            return Auth::user()->user_privilege->is_admin ? view('theme_pages.about_theme', [
                'theme' => $theme,
            ]) : redirect()->back();
        } else
            return redirect('/login');
    }

    public function manage_edit_theme($id)
    {
        $theme = Theme::find($id);
        if (Auth::check()) {
            return Auth::user()->user_privilege->is_admin ? view('theme_pages.edit_theme', [
                'theme' => $theme,
            ]) : redirect()->back();
        } else
            return redirect('/login');
    }

    public function manage_update_theme(ThemeRequest $request, $id)
    {
        $theme = Theme::find($id);
        if ($theme->theme != $request->theme) {
            $theme->theme = $request->theme;
            $theme->save();
            foreach ($theme->posts as $post) {
                $post->theme_id = $theme->id;
                $post->save();
            }
        }
        if (Auth::check()) {
            return Auth::user()->user_privilege->is_admin ? redirect('manage_themes') : redirect()->back();
        } else
            return redirect('/login');
    }

    public function manage_delete_theme($id)
    {
        $theme = Theme::find($id);
        if (Auth::check()) {
            return Auth::user()->user_privilege->is_admin ? view('theme_pages.delete_theme', [
                'theme' => $theme,
            ]) : redirect()->back();
        } else
            return redirect('/login');
    }

    public function manage_destroy_theme($id)
    {
        $theme = Theme::find($id);
        foreach ($theme->posts as $post) {
            $post->theme_id = 1;
            $post->save();
        }
        $theme->delete();
        if (Auth::check()) {
            return Auth::user()->user_privilege->is_admin ? redirect('manage_themes') : redirect()->back();
        } else
            return redirect('/login');
    }
}
