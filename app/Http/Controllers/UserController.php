<?php

namespace App\Http\Controllers;

use App\Http\Requests\UsersUpdateRequest;
use Illuminate\Support\Facades\Auth;
use App\User;

class UserController extends Controller
{
    public function show_profile_info($id)
    {
        $user = User::find($id);
        return Auth::check() ? view('user_pages.about_user', [
            'user' => $user,
        ]) : redirect('/login');
    }

    public function edit_profile_info($id)
    {
        $user = User::find($id);
        return Auth::check() ? view('user_pages.edit_user', [
            'user' => $user,
        ]) : redirect('/login');
    }

    public function update_profile(UsersUpdateRequest $request)
    {
        $user = Auth::user();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = $user->password;
        $user->save();
        if (is_null($request->birth_date) || $request->birth_date == "")
            $request->birth_date = NULL;
        $user->user_info->birth_date = $request->birth_date;
        $user->user_info->telephone = $request->telephone;
        $user->user_info->status = $request->status;
        $user->user_info->user_id = $user->id;
        $user->user_info->save();
        return Auth::check() ? redirect('/home') : redirect('/login');
    }
}
