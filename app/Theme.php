<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Theme extends Model
{
    protected $fillable = [
        'theme',
    ];

    public function posts(){
		return $this->hasMany('App\Post', 'theme_id');
	}
}
