<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	protected $fillable = ['title', 'theme', 'post_content'];

	public function author()
	{
		return $this->belongsTo('App\User', 'user_id');
	}

	public function comments()
	{
		return $this->hasMany('App\Comment', 'post_id');
	}

	public function themes()
	{
		return $this->belongsTo('App\Theme', 'theme_id');
	}
}
