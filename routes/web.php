<?php

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'PostsController@main_page');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/search_res', 'HomeController@index_filtered')->name('index_filtered');

Route::get('/home/create', 'PostsController@create');

Route::post('/home', 'PostsController@store');

Route::resource('posts', 'PostsController');

Route::get('/posts/{id}/delete', 'PostsController@delete')->where('id', '[0-9]+')->name('delete_post');

Route::post('/posts/{id}/add_comment', 'CommentsController@add_comment')->where('id', '[0-9]+')->name('add_comment');

Route::get('/posts/{id}/comment/{comment_id}/edit', 'CommentsController@edit_comment')->where(['id' => '[0-9]+', 'comment_id' => '[0-9]+'])->name('edit_comment');

Route::put('/posts/{id}/comment/{comment_id}/update', 'CommentsController@update_comment')->where(['id' => '[0-9]+', 'comment_id' => '[0-9]+'])->name('update_comment');

Route::get('/posts/{id}/comment/{comment_id}/delete', 'CommentsController@delete_comment')->where(['id' => '[0-9]+', 'comment_id' => '[0-9]+'])->name('delete_comment');

Route::delete('/posts/{id}/comment/{comment_id}/destroy', 'CommentsController@destroy_comment')->where(['id' => '[0-9]+', 'comment_id' => '[0-9]+'])->name('destroy_comment');

Route::get('/profile/{id}', 'UserController@show_profile_info')->where('id', '[0-9]+')->name('profile');

Route::get('/profile/{id}/edit', 'UserController@edit_profile_info')->where('id', '[0-9]+')->name('edit_profile');

Route::put('/profile/{id}/update', 'UserController@update_profile')->where('id', '[0-9]+')->name('update_profile');

Route::get('/manage_users', 'AdminController@show_manage_users')->name('manage_users');

Route::get('/manage_users/create_user', 'AdminController@manage_create_user')->name('manage_create_user');

Route::post('/manage_users/create_user', 'AdminController@manage_add_user')->name('manage_add_user');

Route::get('/manage_users/users/{id}', 'AdminController@manage_about_user')->where('id', '[0-9]+')->name('manage_about_user');

Route::get('/manage_users/users/{id}/edit', 'AdminController@manage_edit_user')->where('id', '[0-9]+')->name('manage_edit_user');

Route::put('/manage_users/users/{id}/update', 'AdminController@manage_update_user')->where('id', '[0-9]+')->name('manage_update_user');

Route::get('/manage_users/users/{id}/delete', 'AdminController@manage_delete_user')->where('id', '[0-9]+')->name('manage_delete_user');

Route::delete('/manage_users/users/{id}/destroy', 'AdminController@manage_destroy_user')->where('id', '[0-9]+')->name('manage_destroy_user');

Route::get('/manage_themes', 'AdminController@show_manage_themes')->name('manage_themes');

Route::get('/manage_themes/themes/{id}', 'AdminController@manage_about_theme')->where('id', '[0-9]+')->name('manage_about_theme');

Route::get('/manage_themes/themes/{id}/edit', 'AdminController@manage_edit_theme')->where('id', '[0-9]+')->name('manage_edit_theme');

Route::put('/manage_themes/themes/{id}/update', 'AdminController@manage_update_theme')->where('id', '[0-9]+')->name('manage_update_theme');

Route::get('/manage_themes/create_theme', 'AdminController@manage_create_theme')->name('manage_create_theme');

Route::post('/manage_themes/create_theme', 'AdminController@manage_add_theme')->name('manage_add_theme');

Route::get('/manage_themes/themes/{id}/delete', 'AdminController@manage_delete_theme')->where('id', '[0-9]+')->name('manage_delete_theme');

Route::delete('/manage_themes/themes/{id}/delete', 'AdminController@manage_destroy_theme')->where('id', '[0-9]+')->name('manage_destroy_theme');
